package com.example.OnlineStoreApp.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String itemName;
    private double price;
    private String description;
    private int quantity;

    public Item(String itemName, double price, String description, int quantity) {
        this.itemName = itemName;
        this.price = price;
        this.description = description;
        this.quantity = quantity;
    }
}
