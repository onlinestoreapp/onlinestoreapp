package com.example.OnlineStoreApp.controller;

import com.example.OnlineStoreApp.models.User;
import com.example.OnlineStoreApp.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserRepo userRepo, BCryptPasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }
    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute(
                "users",
                userRepo.findAll());

        model.addAttribute(
                "currentUser",
                SecurityContextHolder.getContext().getAuthentication().getName());

        return "users";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/register")
    public String register(User user) {

        return "register";
    }

    @PostMapping("/adduser")

    public String save(User user) {

        if (userRepo.findByUsername(user.getUsername()) != null)
            throw new UsernameNotFoundException("Username already taken");

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepo.save(user);


        return "redirect:/";
    }

    @PostMapping("/edituser")

    public String edit(User user) {
        System.out.println(user);
        userRepo.save(user);

        return "redirect:/";
    }

    @GetMapping("/edituser/{id}")
    public String editUser(@PathVariable("id") long id, Model model) {
        model.addAttribute(
                "user",
                userRepo.getOne(id));
        return "edituser";
    }
}
