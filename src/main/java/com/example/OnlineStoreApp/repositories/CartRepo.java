package com.example.OnlineStoreApp.repositories;

import com.example.OnlineStoreApp.models.Cart;
import com.example.OnlineStoreApp.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepo extends JpaRepository<Cart, Long> {
    Cart findCartByUser(User user);
}
