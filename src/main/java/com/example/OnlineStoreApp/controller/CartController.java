package com.example.OnlineStoreApp.controller;

import com.example.OnlineStoreApp.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class CartController {
    private  final UserRepo userRepo;
@Autowired
    public CartController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/cart/{username}")
    public String showCart(@PathVariable("username") String username, Model model) {
        model.addAttribute("cart", userRepo.findByUsername(username).getCart());
        return "cart";
    }
}
