package com.example.OnlineStoreApp;

import com.example.OnlineStoreApp.models.Cart;
import com.example.OnlineStoreApp.models.Item;
import com.example.OnlineStoreApp.models.User;
import com.example.OnlineStoreApp.repositories.ItemRepo;
import com.example.OnlineStoreApp.repositories.UserRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.stream.Stream;

@SpringBootApplication
public class OnlineStoreAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(OnlineStoreAppApplication.class, args);
    }

    @Bean
    CommandLineRunner init(UserRepo userRepository, BCryptPasswordEncoder passwordEncoder, ItemRepo itemRepo) {
        return args -> {
            Stream.of("John", "a", "Julie", "Jennifer", "Helen", "Rachel").forEach(name -> {
                User user = new User(name, name, name.toLowerCase() + "@domain.com",passwordEncoder.encode("aaa"),new Cart());
                userRepository.save(user);
            });
            userRepository.findAll().forEach(System.out::println);
            itemRepo.save(new Item("white  t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("black t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("red t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("green t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("blue t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("purple t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("t-shirt",5.0,"Plain t-shirt",10));
            itemRepo.save(new Item("baseball cap",7.6,"Limited edition cap",2));
            itemRepo.findAll().forEach(System.out::println);
        };
    }

}
