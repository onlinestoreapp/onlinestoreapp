package com.example.OnlineStoreApp.controller;

import com.example.OnlineStoreApp.models.Cart;
import com.example.OnlineStoreApp.models.Item;
import com.example.OnlineStoreApp.models.User;
import com.example.OnlineStoreApp.repositories.CartRepo;
import com.example.OnlineStoreApp.repositories.ItemRepo;
import com.example.OnlineStoreApp.repositories.UserRepo;
import com.example.OnlineStoreApp.service.ItemPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class ShopController {
    private final ItemRepo itemRepo;
    private final UserRepo userRepo;
    private final CartRepo cartRepo;
    private final ItemPageService itemPageService;

    @Autowired
    public ShopController(ItemRepo itemRepo, UserRepo userRepo, CartRepo cartRepo,ItemPageService itemPageService) {
        this.itemRepo = itemRepo;
        this.userRepo = userRepo;
        this.cartRepo = cartRepo;
        this.itemPageService = itemPageService;

    }

    @GetMapping("/shop")
    public String showItems(Model model) {
        model.addAttribute(
                "items",
                itemRepo.findAll());
        model.addAttribute(
                "currentUser",
                SecurityContextHolder.getContext().getAuthentication().getName());

        return "shop";
    }

    @GetMapping("/addToCart/{id}/user/{username}")
    public String addToCart(@PathVariable("id") long id, @PathVariable("username") String username) {
        Item item = itemRepo.getOne(id);


        if (item.getQuantity() > 0) {
            item.setQuantity(item.getQuantity() - 1);
            itemRepo.save(item);
            Cart cart = userRepo.findByUsername(username).getCart();

            cart.getItems().add(item);
            cartRepo.save(cart);
        }
        System.out.println(userRepo.findByUsername(username).getCart());

        return "redirect:/shop";
    }

    @GetMapping("/pagedItems")
    public String pagedItems(Model model,
                             @RequestParam("page") Optional<Integer> page,
                             @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Item> itemPage = itemPageService.findPaginated(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("itemPage", itemPage);
        int totalPages = itemPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "pagedItems";
    }
}
