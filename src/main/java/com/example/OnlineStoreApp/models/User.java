package com.example.OnlineStoreApp.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String username;
    private String name;
    private String surname;
    private String email;
    private String country;
    private String city;
    private String street;
    private String zipCode;
    private String password;

    @OneToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    private Cart cart;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public User(String username, String name, String email, String password, Cart cart) {
        this.username = username;
        this.name = name;
        this.email = email;
        this.password = password;
        this.cart=cart;
    }

    public User() {
    }


}
