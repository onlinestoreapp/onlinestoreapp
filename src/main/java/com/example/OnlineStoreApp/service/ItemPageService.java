package com.example.OnlineStoreApp.service;

import com.example.OnlineStoreApp.models.Item;
import com.example.OnlineStoreApp.repositories.ItemRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class ItemPageService {
    private ItemRepo itemRepo;


    public Page<Item> findPaginated(Pageable pageable) {

        List<Item> items = itemRepo.findAll();


        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Item> list = items;

        if (items.size() < startItem) {
            list = Collections.emptyList();
    }else {
            int toIndex = Math.min(startItem + pageSize, list.size());
            list = list.subList(startItem, toIndex);
        }
        Page<Item> itemPage
                = new PageImpl<Item>(list, PageRequest.of(currentPage, pageSize), items.size());

        return itemPage;

    }
    @Autowired
    public ItemPageService(ItemRepo itemRepo) {
        this.itemRepo = itemRepo;

}
}
